# Project Name
> Validating User Web Table Contents

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Contact](#contact)


## General Information
- In this Project you can add user to table and validate user exist or not


## Technologies Used
- Selenium WebDriver  - version 3.141.59
- Maven sure plugin  - version 2.20
- Extent Reports  - version 4.14
- TestNG  - version 6.8
- log4j  - version 2.8.2


## Features

- Parellel Execution with TestNG Framework
- Automatic Logs Generation with Log4j 
- Automatic Report Generation with Extent Reports - "user.dir")+"\\reports\\index.html"
- Capturing Screenshots automatically on Test Failure


## Screenshots
![Example screenshot]D:\ICT solutions\ICT Solutions Automation Project\reports\\EnsureMyUsersAddedToList.png"


## Setup
Its a Maven Project, all dependencies could be found under pom.xml file

To run this project, make sure JDK, maven installed.


## Usage
To Execute Test cases TestNG framework has been implemented for Parellel execution.
By adding arguments to @Test annotation signature, you can control execution flow. 

`Code Sample`
@Test(priority=2)
public void clickAddUser() throws IOException
{	
	
	LandingPage l=new LandingPage(driver);

	//Clicking Add User Button
    l.getAddUser().click();
    Assert.assertTrue(l.getPopUpAddUser().isDisplayed());
    log.info("Add User Button Clicked Successfully");
	 System.out.println("Clicking Add User Button-Test completed");	
	}


## Project Status
Project is: Completed



## Contact
Created by [Nutan Kumar] 

