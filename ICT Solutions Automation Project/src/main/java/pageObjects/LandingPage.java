package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
//rahulonlinetutor@gmail.com
public class LandingPage {

	
	public WebDriver driver;
	
	By signin=By.cssSelector("a[href*='sign_in']");
	By title=By.cssSelector(".text-center>h2");
	By NavBar=By.cssSelector(".nav.navbar-nav.navbar-right>li>a");
	By myUserName = By.xpath("/html/body/table/tbody/tr/td[contains(.,'Nutanmj')]");
	By firstName1 = By.xpath("/html/body/table/tbody/tr/td[contains(.,'FName1')]");
	By firstName2 = By.xpath("/html/body/table/tbody/tr/td[contains(.,'FName2')]");
	By addUser = By.xpath("//button[@type='add']");
	By popUpAddUser = By.xpath("//div[@class='modal ng-scope']");
	By firstName = By.xpath("//input[@name='FirstName']");
	By lastName = By.xpath("//input[@name='LastName']");
	By userName = By.xpath("//input[@name='UserName']");
	By password = By.xpath("//input[@type='password']");
	By customerAAA = By.xpath("//input[@value=15]");
	By customerBBB = By.xpath("//input[@value=16]");
	By Role = By.xpath("//select[@name='RoleId']");
	By email = By.xpath("//input[@type='email']");
	By cellPhone = By.xpath("//input[@name='Mobilephone']");
	By saveButton = By.xpath("//button[text()='Save']");
	By closeButton = By.xpath("//button[text()='Close']");
	
	
	
	public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}




	public LoginPage getLogin()
	{
		 driver.findElement(signin).click();
		 LoginPage lp=new LoginPage(driver);
		 return lp;		 
		 
		 
		 
	}
	public WebElement getNavigationBar()
	{
		return driver.findElement(NavBar);
	}
	public WebElement getTitle()
	{
		return driver.findElement(title);
	}
	
	
	public WebElement getmyUserName()
	{
		return driver.findElement(myUserName);
	}
	
	public WebElement getAddUser()
	{
		return driver.findElement(addUser);
	}
	public WebElement getPopUpAddUser()
	{
		return driver.findElement(popUpAddUser);
	}
	public WebElement getfirstName()
	{
		return driver.findElement(firstName);
	}
	public WebElement getlastName()
	{
		return driver.findElement(lastName);
	}
	public WebElement getuserName()
	{
		return driver.findElement(userName);
	}
	public WebElement getPassword()
	{
		return driver.findElement(password);
	}
	public WebElement getcustomerAAA()
	{
		return driver.findElement(customerAAA);
	}
	public WebElement getcustomerBBB()
	{
		return driver.findElement(customerBBB);
	}
	public WebElement getemail()
	{
		return driver.findElement(email);
	}
	public WebElement getcellPhone()
	{
		return driver.findElement(cellPhone);
	}
	public WebElement getsaveButton()
	{
		return driver.findElement(saveButton);
	}
	public WebElement getcloseButton()
	{
		return driver.findElement(closeButton);
	}
	public WebElement getfirstName1()
	{
		return driver.findElement(firstName1);
	}
	public WebElement getfirstName2()
	{
		return driver.findElement(firstName2);
	}

	public Select selectRole()
	{
		Select role = new Select(driver.findElement(Role));
		 return role;
		
	}
	
	
	
}
