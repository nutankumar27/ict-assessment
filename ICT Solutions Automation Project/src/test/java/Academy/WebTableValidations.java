package Academy;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import pageObjects.LandingPage;
import pageObjects.LoginPage;
import resources.ExtentReporterNG;
import resources.base;
//Adding logs 
//Generating html reports

public class WebTableValidations extends base{
	ExtentReports extent = ExtentReporterNG.getReportObject();
	public WebDriver driver;	
	 public static Logger log =LogManager.getLogger(base.class.getName());
	 
@BeforeTest

public void initialize() throws IOException
{
	 driver =initializeDriver();		
	driver.get(prop.getProperty("url"));
	driver.manage().window().maximize();
}
	
	
	
@Test(priority=1)
	public void validateIamInTheTable() throws IOException
	{
		
		// creating object to that class and invoke methods of it
		LandingPage l=new LandingPage(driver);
	
		l.getAddUser().click();
		 /************************** Adding Myself to Table ************************************/
	    l.getfirstName().clear();
	    l.getfirstName().sendKeys("Nutan");
	    l.getlastName().clear();
	    l.getlastName().sendKeys("Kumar");
	    l.getuserName().clear();
	    l.getuserName().sendKeys("Nutanmj");
	    l.getPassword().clear();
	    l.getPassword().sendKeys("TestPass");
	    l.getcustomerAAA().click();
	    l.selectRole().selectByVisibleText("Admin");
	    l.getemail().clear();
	    l.getemail().sendKeys("Nutantest@mall.com");
	    l.getcellPhone().clear();
	    l.getcellPhone().sendKeys("87492374");    
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
	    l.getsaveButton().click();	       
		//Validating my User name is displaying in table or not
	    Assert.assertTrue(l.getmyUserName().isDisplayed());
	    log.info("My User Name is displayed");
		 System.out.println("Validating My User name in Table-Test completed");	
		}

@Test(priority=2)
public void clickAddUser() throws IOException
{	
	
	LandingPage l=new LandingPage(driver);

	//Clicking Add User Button
    l.getAddUser().click();
    Assert.assertTrue(l.getPopUpAddUser().isDisplayed());
    log.info("Add User Button Clicked Successfully");
	 System.out.println("Clicking Add User Button-Test completed");	
	}

@Test(priority=3)	
public void AddUser() throws IOException
{	
	
	LandingPage l=new LandingPage(driver);
	//closing previously opened Add user popup window
	l.getcloseButton().click();
	//Clicking Add User Button
    l.getAddUser().click();
    
    /************************** Adding first User ************************************/
    l.getfirstName().clear();
    l.getfirstName().sendKeys("FName1");
    l.getlastName().clear();
    l.getlastName().sendKeys("LName1");
    l.getuserName().clear();
    Random rand = new Random(); //instance of random class
    int upperbound1 = 25;
    int upperbound2 = 25;
      //generate random values from 0-24
    int int_random = rand.nextInt(upperbound1);     
    int int_random2 = rand.nextInt(upperbound2);     
    l.getuserName().sendKeys("User" + int_random);
    l.getPassword().clear();
    l.getPassword().sendKeys("Pass1");
    l.getcustomerAAA().click();
    l.selectRole().selectByVisibleText("Admin");
    l.getemail().clear();
    l.getemail().sendKeys("admin@mall.com");
    l.getcellPhone().clear();
    l.getcellPhone().sendKeys("082555");    
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    l.getsaveButton().click();
    
    /************************** Adding Second User ************************************/
   // Adding another User
    l.getAddUser().click();
    l.getfirstName().clear();
    l.getfirstName().sendKeys("FName2");
    l.getlastName().clear();
    l.getlastName().sendKeys("LName2");
    l.getuserName().clear();     
    l.getuserName().sendKeys("User" + int_random2);
    l.getPassword().clear();
    l.getPassword().sendKeys("Pass2");
    l.getcustomerBBB().click();
    l.selectRole().selectByVisibleText("Customer");
    l.getemail().clear();
    l.getemail().sendKeys("customer@mall.com");
    l.getcellPhone().clear();
    l.getcellPhone().sendKeys("083444");   
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    l.getsaveButton().click();       
    log.info("User added Successfully");
	 System.out.println("Adding User to Table-Test completed");	
	}

@Test(priority=4)
public void EnsureMyUsersAddedToList() throws IOException
{	
	// creating object to that class and invoke methods of it
	LandingPage l=new LandingPage(driver);
	//Validating my User name is displaying in table or not
	String firstname1 = l.getfirstName1().getText();
    Assert.assertTrue(firstname1.equals("FName1"));
    String firstname2 = l.getfirstName2().getText();
    Assert.assertTrue(firstname2.equals("FName1"));
    log.info("My Users are added to List Successfully");
	 System.out.println("Adding users to list -Test completed");	
	}


	
	@AfterTest
	public void teardown()
	{
		
		driver.close();
		
	
		
	}

	
}
